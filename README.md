# XTILE Graphics Library

This is a graphics library that was geared towards making games and animations.  I did have sales for it, and it was used
in actual commercial products.  The world of graphics changed a lot since then.  When SVGA became available, I started 
working on a version for it, but abandoned when hardware acceleration started showing up.  That pretty much 
eliminated any performance advantage this library had.

The library was based on the [VGA Mode X](http://archive.gamedev.net/archive/reference/articles/article373.html).  It was an 
undocumented graphics "mode" that allowed you to move pixels around in graphics memory four bytes at a time.  That 
dramatically improved performance over the other modes.  That along with 8-bit color and 1:1 pixel ratio made it very 
attractive to game developers.

A few years ago I changed the license and pushed it up to my github repo.  I consider it a curiosity from the past rather
than useful software now.  The original documentation is in the DOCUMENTATION.TXT file.  In there you'll find more 
information about my intentions, Mode X and how the library worked.  Unfortunately it is literally the original--version 1.0.
The latest version is 2.1 and contains quite a few improvements, including sprites, scaled sprites, circle and line 
drawing, a mouse driver, significant performance improvements and other stuff.  If I find the newer documentation, 
I will update it in the repo.

Note that this repo contains more files than in the original distribution, including all my testing programs.  I'm told
the .EXE executables run fine in DosBox, but I have no tried it myself.

-Erich


